﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUpdate : MonoBehaviour //Класс отвечаюший за прогрес игры.
{
    public static GameUpdate Signleton { get; private set; }//Быстрый доступ к обьекту

    public event Action WinGame;

    List<FigureCell> figureCells = new List<FigureCell>();
    List<Figure> figures = new List<Figure>();

    private void Awake()
    {
        if (Signleton)
        {
            Destroy(gameObject);
            return;
        }
        Signleton = this;
    }

    public void AddCell(FigureCell cell)//Этот метод вызывают фигуры которые находяться в сцене что б их записало
    {
        figureCells.Add(cell);
    }

    public void AddFigure(Figure figure)//Тоже самое только для фигур
    {
        figures.Add(figure);
    }

    private void Update()//Проверка на то что можно ли установить взятую фигуру в ячейку и закончена ли игра.
    {
        for (int i = 0; i < figures.Count; i++)
        {
            if (figures[i].IsMove)
            {
                for (int i2 = 0; i2 < figureCells.Count; i2++)
                {
                    if (figureCells[i2].IsActive && figureCells[i2].CheckFigure(figures[i]))
                    {
                        figureCells[i2].SetFigure(figures[i]);
                        if (CheckGame())
                        {
                            EndGame();
                        }
                    }
                }
            }
        }
    }

    private bool CheckGame()//проверка игры на завершенность.
    {
        bool isWin = false;
        for (int i = 0; i < figureCells.Count; i++)
        {
            if (figureCells[i].IsActive)
            {
                isWin = true;
                break;
            }
        }
        return !isWin;
    }

    private void EndGame()//Точка выхода из игры.
    {
        Debug.Log("Win");
        if (WinGame != null)
        {
            WinGame();
        }
    }
    private void OnDestroy()
    {
        if (Signleton == this)
            Signleton = null;
    }
}
