﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SideArrow //Направление для вращения
{
    Left,
    Right,
}

public class RotationArrow : MonoBehaviour//Стрелка вращения
{

    public bool IsPress
    {
        get
        {
            return pressButton.IsPress;
        }
    }

    [SerializeField] private SideArrow side;

    [SerializeField] private float speedRotate;

    [SerializeField] private PressButton pressButton;

    private Figure figure;



    public void Hide()//Скрыть стрелку
    {
        pressButton.Press -= Rotate;
        figure = null;
        gameObject.SetActive(false);
    }

    public void SetFigure(Figure setFigure)//Прицепить стрелку к фигуре
    {
        pressButton.Press += Rotate;
        gameObject.SetActive(true);
        figure = setFigure;
    }

    private void Rotate()
    {
        int r = 1;
        if (side == SideArrow.Right)
        {
            r = -1;
        }
        if (pressButton.IsPress)
        {
            figure.AddRotate = 90f * r;
        }
    }

    private void FixedUpdate()
    {
        Vector3 direct = Vector3.zero;
        int r = 1;//Направление вращения
        if (side == SideArrow.Left)
        {
            direct = figure.GetLeftSide();
        }
        else
        {
            direct = figure.GetRightSide();
            r = -1;
        }

        transform.position = figure.transform.position + direct;

        /*  if (pressButton.IsPress)
          {
              figure.AddRotate = speedRotate * Time.fixedDeltaTime * r;
          }*/
    }
}
