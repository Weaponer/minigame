﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeFigure : MonoBehaviour//Класс для взятие вигуры для перетаскивания
{

    [SerializeField] private RotationArrow leftArrow;//ссылки на стрелочки
    [SerializeField] private RotationArrow rightArrow;

    public static Figure FigureTake { get; private set; }


    private static RotationArrow leftA;
    private static RotationArrow rightA;

    private void Awake()
    {
        leftA = leftArrow;
        rightA = rightArrow;
    }
    public static void Take(Figure figure)//Взятие фигуры для перетягивания
    {
        if (FigureTake != null)
        {
            FigureTake.Put();
        }
        if (!figure.IsAnimation && !figure.IsMove && !figure.IsSet)
        {
            FigureTake = figure;
            FigureTake.Take();
            leftA.SetFigure(FigureTake);
            rightA.SetFigure(FigureTake);
            FigureTake.SetArrow(leftA, rightA);
        }
    }

    public static void Put()//Убрать фигуру для перетягивания.
    {
        if (FigureTake != null)
        {
            leftA.Hide();
            rightA.Hide();
            FigureTake.Put();
            FigureTake = null;
        }
    }

    private void OnDisable()
    {
        leftA = null;
        rightA = null;
        FigureTake = null;
    }
}
