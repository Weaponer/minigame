﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Mirrored //Требуймая точность для поворота
{
    AllMirrored,//Любой стороной под 90
    MirroredSide,//Либо прямо, либо верх ногами
    None,//идентично
}

public class FigureCell : MonoBehaviour
{
    public bool IsActive
    {
        get
        {
            return active;
        }
    }

    private bool active = true;

    [SerializeField] private int indexCull;//Номер ячейки ( у ячейки и фигуры должны быть одинаковы).

    [SerializeField] private Vector3 baseRotate;

    [SerializeField] private Mirrored mirrored;

    [SerializeField] private float radiusTake;

    private Figure figure;

    private void Start()
    {
        GameUpdate.Signleton.AddCell(this);
    }


    public bool CheckFigure(Figure figure)//проверка на то можно ли вставить фигуру в ячейку
    {
        if (figure.Index == indexCull && Vector3.Distance(transform.position, figure.transform.position) < radiusTake)
        {
            if (mirrored == Mirrored.None)
            {
                if (Quaternion.Dot(Quaternion.Euler(baseRotate), figure.transform.rotation) > 0.9f)
                {
                    return true;
                }
            }
            else if (mirrored == Mirrored.MirroredSide)
            {
                if (Mathf.Abs(Quaternion.Dot(Quaternion.Euler(baseRotate), figure.transform.rotation)) > 0.99f)
                {
                    return true;
                }
            }
            else if (mirrored == Mirrored.AllMirrored)
            {
                if (Mathf.Abs(Quaternion.Dot(Quaternion.Euler(baseRotate), figure.transform.rotation)) > 0.9f || Mathf.Abs(Quaternion.Dot(Quaternion.Euler(baseRotate), figure.transform.rotation)) < 0.1f)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void SetFigure(Figure figure)//установка фигуры в ячейку
    {
        active = false;
        this.figure = figure;
        figure.SetInCell(transform.position, transform.rotation, transform.localScale);
    }
    private void OnDisable()
    {

    }
}
