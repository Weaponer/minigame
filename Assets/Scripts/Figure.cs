﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class Figure : MonoBehaviour
{
    public int Index //Номер фигуры ( у ячейки и фигуры должны быть одинаковы).
    {
        get
        {
            return index;
        }
    }

    public bool IsMove//Двигаеться ли фигура
    {
        get
        {
            return move;
        }
    }

    public bool IsAnimation//Включена ли анимация у фигуры
    {
        get
        {
            return animationReturn != null;
        }
    }

    public bool IsSet//Установлена ли фигура в ячейку
    {
        get
        {
            return block;
        }
    }

    public float AddRotate//Указать поворот фигуре
    {
        get { return transform.localEulerAngles.x; }
        set { transform.localEulerAngles += new Vector3(0, 0, value); }
    }

    [SerializeField] private int index;

    [SerializeField] private PressButton pressButton;

    [SerializeField] private Vector3 bigScale; //Размер фигуры в взятом виде

    [SerializeField] private Vector3 leftSide;//Левая позиция от фигуры для стрелочки

    [SerializeField] private Vector3 rightSide;//Правая позиция от фигуры для стрелочки

    Stopwatch timerOne = new Stopwatch(); //Таймер

    Coroutine timerReturn;

    Coroutine animationReturn;

    private Vector3 savePos;

    private Quaternion saveRot;

    private Vector3 saveScale;

    private bool block;

    private bool move;

    private RotationArrow leftArrow;
    private RotationArrow rightArrow;

    private void Start()
    {
        GameUpdate.Signleton.AddFigure(this);
        SaveTransorm();
        pressButton.Press += PressFigure;
        pressButton.UnPress += StartTimerReturnPosition;
    }

    private void Update()//установка фигуры в позицию если ие двигают
    {
        if (move && pressButton.IsPress)
        {
            Vector3 pos = CameraController.GameCamera.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);
            pos.z = 0;
            transform.localPosition = pos;
        }
    }

    public void SetInCell(Vector3 position, Quaternion rotation, Vector3 scale)//Установка фигуры в ячейку
    {
        block = true;
        if (move)
        {
            TakeFigure.Put();
        }
        transform.position = position;
        transform.rotation = rotation;
        transform.localScale = scale;

    }

    public Vector3 GetLeftSide()
    {
        return leftSide;
    }

    public Vector3 GetRightSide()
    {
        return rightSide;
    }
    private void SaveTransorm()//Сохраняет нынешнию позицию
    {
        savePos = transform.position;

        saveRot = transform.rotation;

        saveScale = transform.localScale;
    }

    private void PressFigure()//Вызываеться когда по фигуре был сделан клик и идет запрос на то что б ие взяли для перетягивания.
    {
        if (!block && !move)
        {
            TakeFigure.Take(this);
        }
    }

    public void Take()//Этот метод будет вызвал если теперь эта фигура была выбрана для перетягивания
    {
        move = true;
        SetBigScale();
        pressButton.Press += PressFigure;
        pressButton.UnPress += StartTimerReturnPosition;

    }

    public void Put()//Вызываеться при отпускании данной фигуры
    {
        pressButton.Press -= PressFigure;
        pressButton.UnPress -= StartTimerReturnPosition;

        if (!block)
        {
            Return();
        }
        if (timerReturn != null)
        {
            StopCoroutine(timerReturn);
        }
        move = false;
    }

    public void SetArrow(RotationArrow l, RotationArrow r)//Указание ссылки на стрелочки для установки
    {
        leftArrow = l;
        rightArrow = r;
    }

    private void SetBigScale()//Установка размера для перетягивания
    {
        transform.localScale = bigScale;
    }

    private void StartTimerReturnPosition()//Вызываеться при отпускания пальца с фигуры.
    {
        if (!block)
        {
            if (timerReturn == null)
            {
                timerReturn = StartCoroutine(TimerReturn());
            }
        }
    }

    private void Return()//Включаеться анимация возвращения на исходную позицию
    {
        animationReturn = StartCoroutine(AnimationReturn());
    }

    IEnumerator TimerReturn()//Задержка для возвращения
    {
        timerOne.Reset();
        timerOne.Start();
        while (true)
        {
            if (pressButton.IsPress || (leftArrow && leftArrow.IsPress) || (rightArrow && rightArrow.IsPress))
            {
                timerOne.Restart();
            }
            if (timerOne.ElapsedMilliseconds > 3000f)
            {
                timerReturn = null;
                TakeFigure.Put();
                yield break;
            }
            else
            {
                yield return null;
            }
        }
    }


    IEnumerator AnimationReturn()//Анимация вращения
    {
        float time = Vector3.Distance(transform.position, savePos) / 70f;

        float rot = transform.eulerAngles.z;
        /*  for (int i = 0; i < 30; i++)
          {
              transform.eulerAngles = new Vector3(0, 0, rot + (Mathf.Cos((float)i / 3)) * 20f);
              yield return new WaitForSeconds(0.02f);

          } */

        CameraController.ShakingCamera();

        Vector3 pos = transform.position;
        Vector3 scale = transform.localScale;
        Quaternion rotQ = transform.rotation;

        for (int i = 0; i < 90; i++)
        {
            transform.position = Vector3.Lerp(pos, savePos, (float)i / 90f);
            transform.localScale = Vector3.Lerp(scale, saveScale, (float)i / 90f);
            transform.rotation = Quaternion.Lerp(rotQ, saveRot, (float)i / 90f);
            yield return new WaitForSeconds(time / 90);
        }

        animationReturn = null;
    }
}
