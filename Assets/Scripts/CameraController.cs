﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour //Класс для получение доступа к камере
{
    public static Camera GameCamera;

    [SerializeField] private Camera camera;

    static Coroutine animation;

    private static CameraController singleton;

    private void Awake()
    {
        if (GameCamera)
        {
            Destroy(gameObject);
            return;
        }
        singleton = this;
        GameCamera = camera;
    }

    private void OnDestroy()
    {
        if (singleton == this)
        {
            singleton = null;
            GameCamera = null;
        }
    }

    public static void ShakingCamera()
    {
        if (animation == null)
        {
            animation = singleton.StartCoroutine(Shaking());
        }
    }

    static IEnumerator Shaking()
    {
        for (int i = 0; i < 30; i++)
        {
            float move = Mathf.Cos((float)i / 3);
            GameCamera.transform.position = new Vector3(move * 0.1f, -move * 0.1f, -10f);
            yield return new WaitForSeconds(0.005f);
        }
        animation = null;
    }
}
