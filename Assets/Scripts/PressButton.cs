﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressButton : MonoBehaviour //Данный компонент отвечает за нажатие по этому обьекту в определенном радиусе
{
    public bool IsPress //Нажатие
    {
        get
        {
            return isPress;
        }
    }

    public event Action Press;//Вызываеться 1 раз при нажатии
    public event Action UnPress;//Вызываеться 1 раз при разжатии если до этого было нажато.

    [SerializeField] private float radius;

    private bool isPress;
    private void Update()
    {
        if (!isPress && Input.GetMouseButtonDown(0))
        {
            Vector3 pos = CameraController.GameCamera.ScreenToWorldPoint(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);
            pos.z = 0;
            if (Vector3.Distance(pos, transform.position) < radius * transform.localScale.magnitude)
            {
                isPress = true;
                if (Press != null)
                {
                    Press();
                }
            }
        }

        if (isPress && (Input.GetMouseButtonUp(0) || !Input.GetMouseButton(0)))
        {
            isPress = false;
            if (UnPress != null)
            {
                UnPress();
            }
        }
    }
}
